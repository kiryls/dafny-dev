SRC_DIR := ./src
OUT_DIR := ./out

SRCS := $(shell find $(SRC_DIR) -name '*.dfy' -not -name 'Demo.dfy')
DLLS := $(OUT_DIR)/Demo.dll $(SRCS:$(SRC_DIR)/%.dfy=$(OUT_DIR)/%.dll)

all: $(DLLS)
	@echo "\nyou can now run with 'make run'"

run: $(DLLS) 
	@dotnet $<

$(OUT_DIR)/Demo.dll: $(SRC_DIR)/Demo.dfy $(SRCS)
	@echo "verifying and building Demo module:"
	@mkdir -p $(dir $@)
	@dafny build $< --output $@

$(OUT_DIR)/%.dll: $(SRCS)
	@echo "\nverifying and building TopSort module:"
	@mkdir -p $(dir $@)
	@dafny build $< --output $@

.PHONY: clean git
clean:
	rm -rf out

f := .
git:
	git add $f
	git commit -m "$(msg)"
	git push origin main