module TopSort {

  type Label = string

  class Sorting {
    var result: seq<Node>

    constructor()
      ensures result == []
    {
      result := [];
    }
  }

  class Node {
    var lbl: Label
    var next: set<Node>
    ghost var Reach: set<object>

    ghost predicate Valid()
      reads this, Reach
    {
      this in Reach &&

      (forall n :: n in next ==>
                     n in Reach &&
                     n.Reach <= Reach) &&

      (forall n :: n in next ==>
                     this !in n.Reach &&
                     n.Valid())
    }

    ghost predicate Ancestor(v: Node)
      requires Valid()
      reads this, Reach
    {
      this == v || exists n :: n in next && n.Ancestor(v)
    }

    static method with(lbl: Label, next: set<Node>) returns (n: Node)
      requires forall v :: v in next ==> v.Valid()
      ensures n.Valid()
    {
      n := new Node;
      n.lbl := lbl;
      n.next := next;
      n.Reach := {n};

      if next != {} {
        ghost var N := next;
        while N != {}
          decreases N
          invariant n in n.Reach
          invariant forall v :: v in next && v !in N ==>
                                  v in n.Reach &&
                                  v.Reach <= n.Reach
        {
          var v :| v in N;
          N := N - {v};
          n.Reach := n.Reach + v.Reach;
        }
      }
    }
  }

  method topsort(G: set<Node>) returns (s: seq<Node>)
    requires forall u :: u in G ==> u.Valid()
    ensures forall u :: u in G ==> u in s
    ensures forall u :: u in s ==> u.Valid()
    ensures forall i, j :: 0 <= i < j < |s| ==> !s[j].Ancestor(s[i])
  {
    var sorting := new Sorting();

    var R := G;
    while R != {}
      decreases R
      invariant forall u :: u in G && u !in R ==>
                              forall v :: u.Ancestor(v) ==> v in sorting.result

      invariant forall u :: u in sorting.result ==>
                              sorting !in u.Reach &&
                              u.Valid() &&
                              forall v :: u.Ancestor(v) ==> v in sorting.result

      invariant forall i, j :: 0 <= i < j < |sorting.result| ==>
                                 !sorting.result[j].Ancestor(sorting.result[i])
    {
      var v :| v in R;
      R := R - {v};

      if v !in sorting.result {
        visit(v, sorting);
      }
    }

    s := sorting.result;
  }

  method visit(n: Node, sorting: Sorting)
    requires n.Valid()

    requires n !in sorting.result

    requires sorting !in n.Reach

    requires forall u :: u in sorting.result ==>
                           sorting !in u.Reach &&
                           u.Valid() &&
                           forall v :: u.Ancestor(v) ==> v in sorting.result

    requires forall i, j :: 0 <= i < j < |sorting.result| ==>
                              !sorting.result[j].Ancestor(sorting.result[i])

    ensures forall u :: u in old(sorting.result) ==> u in sorting.result
    ensures n.Valid()

    ensures forall u :: n.Ancestor(u) ==> u in sorting.result

    ensures forall u :: u in sorting.result ==>
                          sorting !in u.Reach &&
                          u.Valid() &&
                          (forall v :: u.Ancestor(v) ==> v in sorting.result) &&
                          (u in old(sorting.result) || n.Ancestor(u))

    ensures forall i, j :: 0 <= i < j < |sorting.result| ==>
                             !sorting.result[j].Ancestor(sorting.result[i])

    decreases n.Reach
    modifies sorting
  {
    var N := n.next;
    while N != {}
      decreases N
      invariant n !in sorting.result
      invariant forall u :: u in old(sorting.result) ==> u in sorting.result

      invariant forall u :: u in n.next ==> u in N || u in sorting.result

      invariant forall u :: u in sorting.result ==>
                              sorting !in u.Reach &&
                              u.Valid() &&
                              (forall v :: u.Ancestor(v) ==> v in sorting.result) &&
                              (u in old(sorting.result) || n.Ancestor(u))

      invariant forall i, j :: 0 <= i < j < |sorting.result| ==>
                                 !sorting.result[j].Ancestor(sorting.result[i])
    {
      var v :| v in N;
      N := N - {v};

      if v !in sorting.result {
        visit(v, sorting);
      }
    }
    sorting.result := [n] + sorting.result;
  }
}