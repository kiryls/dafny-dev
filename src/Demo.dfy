include "TopSort.dfy"

module Demo {
  import opened TopSort

  method PrintGraph(G: set<Node>)
  {
    print "\nGraph:\n";

    var H := G;
    while H != {}
    {
      var u :| u in H;
      H := H - {u};

      print u.lbl, ": [";

      var N := u.next;
      var first := true;
      while N != {}
      {
        var n :| n in N;
        N := N - {n};

        if first {
          print n.lbl;
          first := false;
        } else {
          print ", ", n.lbl;
        }

      }

      print "]\n";
    }
  }

  method PrintSequence(s: seq<Node>)
  {
    print "\nTopological order:\n";

    print "[";

    for i := 0 to |s| {
      if i == 0 {
        print s[i].lbl;
      } else {
        print ", ", s[i].lbl;
      }
    }

    print "]\n\n";
  }

  method Main() {
    var jacket := Node.with("Jacket", {});
    var tie := Node.with("Tie", {jacket});
    var belt := Node.with("Belt", {jacket});
    var shirt := Node.with("Shirt", {belt, tie});
    var watch := Node.with("Watch", {});
    var shoes := Node.with("Shoes", {});
    var pants := Node.with("Pants", {belt, shoes});
    var undershorts := Node.with("Undershorts", {pants, shoes});
    var socks := Node.with("Socks", {shoes});

    var DAG := {belt, jacket, pants, shirt, shoes, socks, tie, undershorts, watch};
    PrintGraph(DAG);

    var s := topsort(DAG);
    assert forall i,j :: 0 <= i < j < |s| ==> !s[j].Ancestor(s[i]);
    PrintSequence(s);
  }
}