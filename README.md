# Dafny Topological Sort

## Overview

This project uses [Dafny](https://dafny.org/), a language equipped with a sophisticated static verification mechanisms, to prove the correctness and completeness of [topological sort](https://en.wikipedia.org/wiki/Topological_sorting) applied to [DAGs](https://en.wikipedia.org/wiki/Directed_acyclic_graph) (Directed Acyclic Graph).

Topological sorting is a linear ordering of the nodes in a DAG such that for every directed edge (u, v), node u comes before node v in the ordering.

## Table of Contents

- [Features](#features)
- [Theory](#theory)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
- [License](#license)
- [Acknowledgments](#acknowledgments)

## Features

Correctness and Completeness proof of the topological sort using dafny's specification statements.

## Theory

In order to prove that a topological sorting is possible on any DAG, you have to provide proofs about many other properties.

**Some of them are:**

1. Every non empty DAG has at least one source node and at least one terminal node
2. Every subgraph of a DAG is itself a DAG (for instance, this property should be proved conservative upon the removal of elements from the DAG)
3. If you can trace a path of length greater then the cardinality of vertices set, the graph is not acyclic
4. A path which has no duplicate nodes has a length <= |V|

## Getting Started

To use this project locally or contribute to its development, follow the instructions below.

### Prerequisites

- Dafny installed on your machine. Follow the installation steps described on [Dafny Github Install Wiki](https://github.com/dafny-lang/dafny/wiki/INSTALL#visual-studio-code).
- You can also download Dafny from [Dafny GitHub Releases](https://github.com/dafny-lang/dafny/releases).

### Installation

1. Clone the repository, including the submodule:

   ```bash
   git clone https://github.com/your-username/dafny-topological-sort.git
   git submodule add https://github.com/dafny-lang/libraries.git dafnylang_libraries
   ```

   - If the submodule introduces breaking changes and this project doesn't run, you can clone [this specific commit](https://github.com/dafny-lang/libraries/tree/ae8708c091d32383235d5d8c15c08cff05613bbc) in a .zip and put the contents inside of `<project_folder>/dafny_libraries`. Just make sure you have the dafny libraries at `ae8708c0` before downloading.
   - To effectively remove the previously added git submodule, run these:
     - Delete the "dafnylang_libraries" section from the `.gitmodules` file
     - Stage `.gitmodules` file with this command: `git add .gitmodules`
     - Delete the "dafnylang_libraries" section from `.git/config` file
     - Run `git rm --cached ./dafny_libraries` (make sure there's no trailing slash)
     - Run `rm -rf .git/modules/dafny_libraries` (make sure there's no trailing slash)
     - Delete the folder in your project with `rm -rf ./dafny_libraries`
     - Commit if you want `git commit -m "Removed dafny_libraries submodule"`
     - Now you're good to download the .zip file from a specific commit and put into the project under the `dafny_libraries` folder

## Usage

1. cd into the cloned folder:

```bash
cd <path_to_project>
```

2. Build the project (it could take a couple of minutes to compile all the modules). You can skip this step if you want to build and run automatically

```bash
make # skip this step to automatically build & run
make run
```

The project's driver is `src/Demo.dfy`, feel free to tinker with it, adding your own graphs.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

Email me [here](mailto:kiryl92@gmail.com).<br>
This is a project made for a Bachelor degree in Information Technology at [University of Turin](http://www.di.unito.it/do/home.pl).
